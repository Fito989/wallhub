class Post{
    post_token;
    message;
    media;
    comment;
    reactions;
    publishDate;
    publishUser;
    releaseDate;


    constructor(post_token, message, media, comment, reactions, publishDate, publishUser, releaseDate) {
        this.post_token = post_token;
        this.message = message;
        this.media = media;
        this.comment = comment;
        this.reactions = reactions;
        this.publishDate = publishDate;
        this.publishUser = publishUser;
        this.releaseDate = releaseDate;
    }
}