export class DataBaseFunctions{
    StartDB(){
        const admin = require('firebase-admin');

        let serviceAccount = require('./bd-wallhub-firebase-adminsdk-mcm5y-ca63e07cdd.json');

        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount)
        });
        return admin.firestore();
    }
    createUser(nickname, firstName, lastName, phone, email, birthDate, password, biography, language, avatar){
        let db = this.StartDB();
        let docRef = db.collection('users').doc(email);
        let insertUser = docRef.create({
            nickname: nickname,
            first_name: firstName,
            last_name: lastName,
            phone: phone,
            email: email,
            birth_date: birthDate,
            password: password,
            biography: biography,
            language: language,
            avatar: avatar
        });
    }
    editUser(nickname, firstName, lastName, phone, email, birthDate, password, biography, language, avatar, user){
        let db = this.StartDB()
        let docRef = db.collection('users').doc(email);
        let insertUser = docRef.set({
            nickname: nickname == null ? user.nickname : nickname,
            first_name: firstName == null ? user.first_name : firstName,
            last_name: lastName == null ? user.last_name : lastName,
            phone: phone == null ? user.phone : phone,
            email: email == null ? user.email : email,
            birth_date: birthDate == null ? user.birth_date : birthDate,
            password: password == null ? user.password : password,
            biography: biography == null ? user.biography : biography,
            language: language == null ? user.language : language,
            avatar: avatar == null ? user.avatar : avatar
        });
    }
    deleteUser(email){
        let db = this.StartDB()
        let docRef = db.collection('users').doc(email);
        let insertUser = docRef.delete();
    }
}