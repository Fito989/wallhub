class Comment {
    comment_token
    text
    comment_user
    comment_date


    constructor(comment_token, text, comment_user, comment_date) {
        this.comment_token = comment_token;
        this.text = text;
        this.comment_user = comment_user;
        this.comment_date = comment_date;
    }
}