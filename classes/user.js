class User {
    user_token;
    nickname;
    firs_name;
    last_name;
    phone;
    email;
    birth_date;
    password;
    friend_list;
    avatar;
    follow_list;
    language;
    biography;
    is_active;


    constructor(user_token, nickname, firs_name, last_name, phone, email, birth_date, password, friend_list, avatar, follow_list, language, biography, is_active) {
        this.user_token = user_token;
        this.nickname = nickname;
        this.firs_name = firs_name;
        this.last_name = last_name;
        this.phone = phone;
        this.email = email;
        this.birth_date = birth_date;
        this.password = password;
        this.friend_list = friend_list;
        this.avatar = avatar;
        this.follow_list = follow_list;
        this.language = language;
        this.biography = biography;
        this.is_active = is_active;
    }
}