const DataBase = require('DB_Config');

let button = document.getElementById('register_button')

function registerButtonAction(){
    let nickname = document.getElementById('form_nickname').value;
    let firstName = document.getElementById('form_firstname').value;
    let lastName = document.getElementById('form_lastname').value;
    let phone = document.getElementById('form_phone').value;
    let birthdate = document.getElementById('form_birthdate').value;
    let email = document.getElementById('form_email').value;
    let passwd = document.getElementById('form_password').value;
    let repeatPasswd = document.getElementById('form_repassword').value;

    console.log(nickname);

    DataBase.DB_Config.createUser(nickname, firstName, lastName, phone, email, birthdate, passwd);

    console.log(nickname);
}