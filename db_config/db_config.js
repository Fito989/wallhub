class DataBaseFunctions{
    static StartDB(){
        const admin = require('firebase-admin');

        let serviceAccount = require('./bd-wallhub-firebase-adminsdk-mcm5y-ab64ada497.json');


        if (!admin.apps.length) {
            admin.initializeApp({
                credential: admin.credential.cert(serviceAccount)
            });
        }

        return admin.firestore();
    }
    static createUser(nickname, firstName, lastName, phone, email, birthDate, password, biography, language, avatar){
        let db = this.StartDB();
        const data = {
            nickname: nickname == null ? null : nickname.toString(),
            first_name: firstName == null ? null : firstName.toString(),
            last_name: lastName == null ? null : lastName.toString(),
            phone: phone == null ? null : phone.toString(),
            email: email == null ? null : email.toString(),
            birth_date: birthDate == null ? null : birthDate.toString(),
            password: password == null ? null : password.toString(),
            biography: biography == null ? null : biography.toString(),
            language: language == null ? null : language.toString(),
            avatar: avatar == null ? null : avatar.toString()
        }
        let docRef = db.collection('users').doc(email.toString()).set(data);
    }
    static editUser(nickname, firstName, lastName, phone, email, birthDate, password, biography, language, avatar, user){
        let db = this.StartDB()
        let docRef = db.collection('users').doc(email);
        let insertUser = docRef.set({
            nickname: nickname == null ? user.nickname : nickname,
            first_name: firstName == null ? user.first_name : firstName,
            last_name: lastName == null ? user.last_name : lastName,
            phone: phone == null ? user.phone : phone,
            email: email == null ? user.email : email,
            birth_date: birthDate == null ? user.birth_date : birthDate,
            password: password == null ? user.password : password,
            biography: biography == null ? user.biography : biography,
            language: language == null ? user.language : language,
            avatar: avatar == null ? user.avatar : avatar
        });
    }
    static deleteUser(email){
        let db = this.StartDB()
        let docRef = db.collection('users').doc(email);
        let insertUser = docRef.delete();
    }
}

exports.DB_Config = DataBaseFunctions;